# How to fix the "Could not enable the discrete graphics card" error of Optirun on Manjaro 18.1.1 (Gnome) Kernel 5.3

## **__Before you start: Make a backup of your data and system!__**

### 1. Step:
- Open the Manjro-Settings-Manager
- Go to "Hardwareconfiguration"
- Choose "Auto Install Proprietary Driver"
- **Do not uninstall video-linux!**
### 2. Step:
- Check your groups: ```groups```
    - If you **are not** in a group called "bumblebee", go to the Manjaro-Settings-Manager -> User Accounts -> *your Account* -> Account type -> show groups and enable the bumblebee group
    - If you **are** in a group called "bumblebee", everything should be okay
### 3. Step:
- Open ```/etc/defaut/tlp``` as root and edit the ```#RUNTIME_PM_BLACKLIST="…"``` variable to ```RUNTIME_PM_BLACKLIST="01:00.0``` (should be the NVIDIA graphics card check with ```lspci | grep "NVIDIA"```)
- edit the ```RUNTIME_PM_DRIVER_BLACKLIST="…"``` variable to ```RUNTIME_PM_DRIVER_BLACKLIST="nouveau nvidia"``` (all these vars can have multible contents, seperated by whitespaces; if the vars already uncommented and edited, **add** the new contents)
### 4. Step:
- Open ```/etc/bumblebee/bumblebee.conf``` and set the PMMethod var in the ```[diver-nvidia]``` module to ```none```
### 5. Step:
- reboot your system
### 6. Step:
- If Xorg refuses to start (```no screens found``` for example, make sure you have the ```video-intel``` driver installed; If nothing helps, restore the backup)
- If Xorg starts and you don't face problems, run ```optirun glxspheres64``` -  Error should be gone.
 
### My system specs:
 ```bash
[felixq@felix-pc-dell ~]$ neofetch
██████████████████  ████████   felixq@pc-dell 
██████████████████  ████████   -------------------- 
██████████████████  ████████   OS: Manjaro Linux x86_64 
██████████████████  ████████   Host: G3 3779 
████████            ████████   Kernel: 5.3.6-1-MANJARO 
████████  ████████  ████████   Uptime: 34 mins 
████████  ████████  ████████   Packages: 1244 (pacman), 45 (flatpak) 
████████  ████████  ████████   Shell: bash 5.0.11 
████████  ████████  ████████   Resolution: 1920x1080 
████████  ████████  ████████   DE: GNOME 3.34.1 
████████  ████████  ████████   WM: Mutter 
████████  ████████  ████████   WM Theme: Adwaita 
████████  ████████  ████████   Theme: Adwaita-dark [GTK2/3] 
████████  ████████  ████████   Icons: Papirus-Dark [GTK2/3] 
                               Terminal: gnome-terminal 
                               CPU: Intel i7-8750H (12) @ 4.100GHz 
                               GPU: NVIDIA GeForce GTX 1050 Ti Mobile 
                               GPU: Intel UHD Graphics 630 
                               Memory: 3490MiB / 15856MiB 
```

## **Good luck!**